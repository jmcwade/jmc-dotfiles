" ============================================================================
" my current all-purpose ~/.vimrc
"
" for more info on any of this, just ask vim for :help
" (and you shall receive that help, in far more detail than you ever imagined)
"
" John R. McWade <jmcwade@gmail.com>
" ============================================================================

" vi-compatible mode...no thanks!
"   in a way, this is a little pointless if its in an individual vimrc
"   as the act of finding such a file turns compatible off, but it can
"   be good in a system-wide vimrc or gvimrc, and doesnt hurt imo to 
"   have it as the first thing in a userspace vimrc.
set nocompatible

" define special string <Leader> for use in mappings
let mapleader=","

" -----------------------------------------------------------
"   Vundle
"     Please see https://github.com/VundleVim/Vundle.vim for 
"     more in-depth configuration details and usage
"     (If you don't use Vundle, just nuke this whole section)
"     NB: assumes a Vundle install path under ~/.vim/bundle/
" -----------------------------------------------------------
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

" ---- plugins i use at the moment (A-Z)
Plugin 'ARM9/arm-syntax-vim'
Plugin 'itchyny/lightline.vim'
Plugin 'preservim/nerdtree'
Plugin 'tpope/vim-fugitive'
" ---- /plugins i use

call vundle#end()
" --------------------------------------------------------------
"   /vundle
" --------------------------------------------------------------

" Turn on filetype detection
"   Also enables plugin files for handling specific filetypes
"   Also loads indent file for specific filetypes
filetype plugin indent on

" -------------------------------------------------------------
"  plugin-specific
" -------------------------------------------------------------
" lightline, for my status bar to show up
set laststatus=2

" Open NERDTree at start if no file arg provided
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" If only thing open is a nerdtree, close
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

nnoremap <Leader>f :NERDTreeToggle<Enter>   " remap NERDTreeToggle
let NERDTreeMinimalUI=1
let NERDTreeQuitOnOpen=1                    " when file is opened, close the tree
let NERDTreeShowHidden=1                    " show hidden files
let NERDTreeNodeDelimiter="\u00a0"          " delimiter separating file/dir from rest of line
" -------------------------------------------------------------
"   /plugin-specific
" -------------------------------------------------------------

" -------------------------------------------------------------
"   buffers and split-panes
" -------------------------------------------------------------
" Hide buffers instead of closing them out.
"     Be cautious though not to lose unsaved changes in hidden buffers 
"     when quitting
set hidden

" Have terminal show what buffer you are editing, if the terminal supports it
set title

" Switch buffers by hitting F5 and a buffer number.
"     From vimtips wiki, but I prefer :ls! instead of :buffer
nnoremap <F5> :ls!<CR>:buffer<Space>

" When opening a vertical split-pane, open new pane to the right.
set splitright

" When opening a horizontal split-pane, open it below the current one.
set splitbelow

" Traverse open panes with Ctrl+arrowkey
"     Intentionally similar to pane-switching mapping I use in tmux
"     Basically my tmux has Alt+arrowkey and vim has Ctrl+arrowkey
nnoremap <C-Left>  <C-W><C-H>
nnoremap <C-Right> <C-W><C-L>
nnoremap <C-Down>  <C-W><C-J>
nnoremap <C-Up>    <C-W><C-K>

" -------------------------------------------------------------
"   /buffers and split-panes
" -------------------------------------------------------------

" -------------------------------------------------------------
"   tabs and spaces
" -------------------------------------------------------------
" A tab inserts spaces instead
"     I'm not here to start any controversies though. If it's not for you,
"     just comment out / delete from the file / or use 'noexpandtab' instead
set expandtab

" Number of spaces that a <Tab> counts for
"     (Clearly related to expandtab above)
"     Type :help tabstop for some interesting details about tabbing strategies
set tabstop=4

" Number of spaces a <Tab> counts for during editing operations
set softtabstop=4

" Num spaces used for each step of an indent (or an autoindent)
set shiftwidth=4

" Round indent to multiple of shiftwidth
set shiftround
" -------------------------------------------------------------
"   /tabs and spaces
" -------------------------------------------------------------

" Set the backspace behaviors of the backspace and delete buttons.
"     Also effects CTRL-W and CTRL-U in Insert mode. As reminder...
"         (CTRL-W deletes word before cursor)
"         (CTRL-U deletes all entered characters before cursor)
"     'indent' allows backspacing over autoindent
"     'eol'    allows backspacing over line breaks
"     'start'  allows backspacing over the start of insert
set backspace=indent,eol,start

" set a colorscheme
syntax on
colorscheme slate

" Set cursor position
"     not really necessary for me though if using a powerline-type plugin
"     which is lightline in my case
set ruler

" Enable mouse in certain terminals
set mouse=a

" How many commands to keep in history.
set history=1000

" Maximum changes that can be rolled-back.
set undolevels=50

" Show relative line numbers in left gutter
set relativenumber

" Max width of text being inserted
set textwidth=80

" Right margin displayed. Doesn't work everywhere
"     Relative number is added to textwidth above
"set colorcolumn=+1

" Set color of the colorcolumn by number.
"     To see a color list type :help ctermbg or
"     :help guibg for gVim
"highlight ColorColumn ctermbg=8

" Long lines all on one line. Comment out or change to 'wrap' if desired
set nowrap

" Turn off wrapping while typing because it is annoying
set formatoptions-=t

"highlight search results
set hlsearch

"real-time search as you type; no waiting for <CR>
set incsearch

"case-insensitive searches
set ignorecase

"case-sensitive search when search pattern contains an A-Z
set smartcase

"reload/source .vimrc each time we save it
"....as seen in bestofvim and other places
if has('autocmd')
    augroup source_vimrc "{
        autocmd!
        autocmd BufWritePost $MYVIMRC source $MYVIMRC
    augroup END "}
endif
